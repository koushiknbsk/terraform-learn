provider "aws" {
    region = "us-east-2"
}

variable "subnet_cidr_block" {
    description = "subnet_cidr_block"
  
}
resource "aws_vpc" "development-vpc" {
    cidr_block = "10.0.0.0/16"
    tags = {
        Name: "development"
    }
}

resource "aws_subnet" "development-subnet-1" {
    vpc_id = aws_vpc.development-vpc.id
    cidr_block = var.subnet_cidr_block 
    availability_zone = "us-east-2a"
    tags = {
        Name: "subnet-dev-1"
    }
}

data "aws_vpc" "existing-vpc" {
    default = true
}

resource "aws_subnet" "development-subnet-2" {
    vpc_id = data.aws_vpc.existing-vpc.id
    cidr_block = "172.31.48.0/20" 
    availability_zone = "us-east-2a"
    tags = {
        Name: "subnet-dev-2"
    }
}

output "dev_vpc_id" {
  value = aws_vpc.development-vpc.id
}

output "dev-subnet-id" {
    value = aws_subnet.development-subnet-1.id
}